## Willkommen auf der Mastodon-Instanz für religiöse Bildung!
## Welcome to the Mastodon instance on religious education!


Alle Menschen, die sich für religiöse Bildung interessieren und engagieren wollen, sind bei uns willkommen. 
Wir möchten einen respektvollen Umgang miteinander pflegen und gute Gastgeberinnen und Gastgeber sein. Sollte uns das einmal nicht gelingen, geht bitte nicht einfach nur still weg, sondern sprecht uns bitte an, per Mail an [reliverse@comenius.de](mailto:reliverse@comenius.de) oder per Nachricht an den Account [@joerglohrer@reliverse.social](https://reliverse.social/@joerglohrer), damit wir sicher stellen können, dass sich alle Beteiligten bei uns wohlfühlen und einbringen können.


### Impressum:

Anbieter von reliverse.social und für die Datenverarbeitung durch diese Mastodon-Instanz verantwortlich ist das:

Comenius-Institut  
Evangelische Arbeitsstätte für Erziehungswissenschaft e.V.  
Vertreten durch:  
Prof. Dr. Dr. h.c. Friedrich Schweitzer, Vorsitzender des Vorstands  
Dr. Jens Dechow, Direktor

Schreiberstraße 12  
48149 Münster  
Telefon 02 51 / 9 81 01 – 0  
Telefax  02 51 / 9 81 01 – 50  
E-Mail [info@comenius.de](mailto:info@comenius.de)

Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz: DE 126 043 813

Für eigene Informationen (d.h. für Inhalte der erklärenden Seiten und des User-Interface sowie für unsere Vereins-Accounts) sind wir gemäß § 7 Abs. 1 TMG nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8-10 TMG sind wir als Diensteanbieter aber nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine Haftung und Verantwortlichkeit ist jedoch erst möglich, sobald wir von einer Rechtsverletzung Kenntnis erhalten. Dann werden wir umgehend die nötigen Maßnahmen ergreifen.

Das Comenius-Institut hat einen **örtlich Beauftragten für den Datenschutz** bestellt. Die Kontaktadresse ist hierfür [datenschutz@comenius.de](mailto:datenschutz@comenius.de).