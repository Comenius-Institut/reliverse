# Hinweise zu reliverse.social

[reliverse.social](https://reliverse.social) ist eine [Mastodon-Instanz](https://joinmastodon.org). Wir vom Comenius-Institut, Evangelische Arbeitsstätte für Erziehungswissenschaft e.V. stellen dir diese Instanz uneigennützig zur Verfügung. Du kannst reliverse.social nutzen, wenn du unsere Nutzungsbedingungen und unseren Verhaltenskodex anerkennst. Hier findest du folgende Hinweise zu unserem Angebot:

1. [Impressum und Verantwortlichkeit](#impressum)
2. [Nutzungsbedingungen](#nutzungsbedingungen)
3. [Verhaltenskodex](#verhaltenskodex)
4. [Datenschutzerklärung](#datenschutz)

<h2 id="impressum">1. Impressum</h2>

Anbieter von reliverse.social und für die Datenverarbeitung durch diese Mastodon-Instanz verantwortlich ist das:

Comenius-Institut  
Evangelische Arbeitsstätte für Erziehungswissenschaft e.V.  
Vertreten durch:  
Prof. Dr. Dr. h.c. Friedrich Schweitzer, Vorsitzender des Vorstands  
Dr. Jens Dechow, Direktor

Schreiberstraße 12  
48149 Münster  
Telefon 02 51 / 9 81 01 – 0  
Telefax  02 51 / 9 81 01 – 50  
E-Mail [info@comenius.de](mailto:info@comenius.de)

Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz: DE 126 043 813

Für eigene Informationen (d.h. für Inhalte der erklärenden Seiten und des User-Interface sowie für unsere Vereins-Accounts) sind wir gemäß § 7 Abs. 1 TMG nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8-10 TMG sind wir als Diensteanbieter aber nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine Haftung und Verantwortlichkeit ist jedoch erst möglich, sobald wir von einer Rechtsverletzung Kenntnis erhalten. Dann werden wir umgehend die nötigen Maßnahmen ergreifen.

Das Comenius-Institut hat einen **örtlich Beauftragten für den Datenschutz** bestellt. Die Kontaktadresse ist hierfür [datenschutz@comenius.de](mailto:datenschutz@comenius.de).

<h2 id="nutzungsbedingungen">2. Nutzungsbedingungen</h2>

### 2.1 Dein Nutzungsverhältnis

Wir bieten [reliverse.social](https://reliverse.social) schwerpunktmäßig, aber nicht ausschließlich, als Plattform zum Austausch über religiöse Bildung an. Du kannst Beiträge lesen, indem du unsere Mastodon-Instanz ohne Benutzerkonto aufrufst. Wenn du unsere Nutzungsbedingungen und unseren Verhaltenskodex anerkennst, kannst du **für dich persönlich, für deine Lerngruppe, deinen Verein, deinen Verband o.ä. ein Benutzerkonto anlegen** und Beiträge über [reliverse.social](https://reliverse.social) ins Mastodon-Netzwerk posten (und über das Mastodon-Netzwerk hinaus ins Fediverse, in dem dienstübergreifend Beiträge ausgetauscht werden).

Das Anlegen von Benutzerkonten erfolgt moderiert. D.h. nach deiner Registrierung musst du von einer/einem unserer Moderator:innen freigeschaltet werden, um [reliverse.social](https://reliverse.social) nutzen zu können. Wir können die Möglichkeit zum Anlegen von Benutzerkonten weiter beschränken, z.B. dass du vor dem Anlegen eines Kontos per E-Mail einen Einladungslink anforderst. Dies hat den Zweck, automatische Registrierungen durch Spambots o.ä. zu verhindern und die Einhaltung des Verhaltenskodexes zu gewährleisten.

Die Freischaltung deines Benutzerkontos erfolgt so schnell wie möglich. Bitte habe Verständnis dafür, dass diese Instanz mit ihren Moderationskräften wächst, die das hauptsächlich ehrenamtlich und ihrer Freizeit tun. Deshalb können wir keine Bearbeitungsfristen nennen.

Wenn du **jünger als 18 Jahre** bist, müssen deine **Eltern** deinem Nutzungsverhältnis für [reliverse.social](https://reliverse.social) **zustimmen**. Wir schicken dir gerne ein Formular, mit dem du die Zustimmung nachweisen kannst – oder Formulare für deine ganze Jugendgruppe. Wende dich dazu bitte per E-Mail an [reliverse@comenius.de](mailto:reliverse@comenius.de).

### 2.2 Finanzierung und Haftung für den Betrieb

Wir verkaufen keine Werbung und keine Daten unserer Nutzer:innen. Du bist hier nicht das Produkt, sondern Teil einer Gemeinschaft. Der Betrieb von [reliverse.social](https://reliverse.social) wird auch durch **ehrenamtliches Engagement** getragen und durch **Spenden** finanziert. Der Finanzierungsstand ist auf unserer Projektseite `HIER ERGÄNZEN` einsehbar. Wir laden dich herzlich ein, zu spenden um die Finanzierung nachhaltig zu sichern. Du musst aber nicht spenden. Die Nutzung von [reliverse.social](https://reliverse.social) erfolgt unentgeltlich.

Der gemeinschaftliche Betrieb durch das Comenius-Institut als eingetragener und gemeinnütziger Verein soll den nachhaltigen Bestand der Instanz und die Verfügbarkeit der Daten sichern. **Die Registrierung als Nutzer:in begründet jedoch keinen Rechtsanspruch auf die Aufrechterhaltung dieses Dienstes, auf die Verfügbarkeit von Daten und keine Garantie für die vollständige und dauerhafte Speicherung und Übermittlung der Daten.** Die Haftung für Schäden, die aus der Nutzung des Dienstes entstehen, wird bei Verschulden durch das Comenius-Institut Evangelische Arbeitsstätte für Erziehungswissenschaft e.V. und seine Mitarbeitenden auf Vorsatz und grobe Fahrlässigkeit beschränkt.

### 2.3 Verantwortung für Beiträge und Einschränkungen der Nutzung

Für alle Texte, Bilder, Videos etc. in deinen Beiträgen bist du verantwortlich. **Achte deshalb bei deiner Nutzung von [reliverse.social](https://reliverse.social) selbst auf die Einhaltung dieser Bedingungen, auf den Verhaltenskodex und auf Gesetze,** insbesondere auf Urheberrechte für Texte, Bilder, Musik und Videos, auf Datenschutz und Persönlichkeitsrechte (z.B. bei Bildern) sowie auf Gesetze zum Jugendschutz.

Wir überwachen dich, dein Nutzerkonto und deine Beiträge nicht. Gemäß §§ 8-10 Telemediengesetz sind wir nicht für fremde Informationen verantwortlich, solange wir keine Kenntnis davon haben. Wir werden aber unverzüglich tätig, sobald wir Kenntnis von problematischen Informationen erlangen. **Wir behalten uns vor, Nutzungsmöglichkeiten einzuschränken,** Beiträge zu entfernen, hochgeladene Dateien zu löschen und / oder Nutzerkonten zu deaktivieren, wenn Nutzer:innen

- gegen diese Bedingungen verstoßen,
- gegen Gesetze der Bundesrepublik Deutschland verstoßen oder
- unseren Verhaltenskodex nicht beachten.

Wir behalten uns auch vor, den Datenaustausch mit anderen Mastodon-Instanzen und sonstigen Servern im Fediverse einzuschränken, wenn diese Instanzen und Server Verhaltensweisen fördern, die gegen unsere Nutzungsbedingungen, gegen Gesetze der Bundesrepublik Deutschland oder gegen unseren Verhaltenskodex verstoßen.

Wenn problematisches Verhalten schnell gestoppt werden muss, können wir ohne Vorankündigung Maßnahmen ergreifen.

### 2.4 Vertrauliche Kommunikation

Du kannst durch Direktnachrichten mit einzelnen Nutzer:innen kommunizieren. Wir lesen diese Nachrichten nicht und geben sie nicht weiter, solange dies nicht aus technischen oder rechtlichen Gründen (z.B. für die Verfolgung von Straftaten durch Behörden) erforderlich ist. Nachrichten an andere Nutzer:innen dieser Instanz sind vermutlich besser geschützt als Nachrichten auf vielen kommerziellen Plattformen (bei Nachrichten an Nutzer:innen anderer Instanzen haben wir leider keinen Einfluss auf den Umgang der anderen Instanzbetreiber mit den Nachrichten). Aber Nachrichten auf Mastodon werden nur für den Transport zwischen Nutzer:innen und Mastodon-Instanz verschlüsselt.

Vertrauliche Informationen solltest du so verschlüsseln, dass sie unter allen Umständen vertraulich bleiben und auch von Serverbetreibern nicht entschlüsselt werden können. Das ist bei Mastodon nicht möglich, deshalb darfst du **[reliverse.social](https://reliverse.social) nicht zur Übermittlung vertraulicher Informationen verwenden.** Wir empfehlen für vertrauliche Kommunikation unseren Messenger [matrix.rpi-virtuell.de](https://matrix.rpi-virtuell.de), du kannst aber auch andere Messenger, z.B. [Signal](https://signal.org/), [Threema](https://threema.ch) oder [DeltaChat](https://delta.chat/de/) nutzen.

<h2 id="verhaltenskodex">3. Verhaltenskodex</h2>

### 3.1 Grundsätzliches

Uns ist eine Umgebung und Atmosphäre wichtig, die alle Kommunikationspartner:innen als einladend, inklusiv und respektvoll wahrnehmen können. Wir erwarten von allen Personen, die über unsere Instanz im Fediverse kommunizieren, dass sie zu einer solchen Umgebung und Atmosphäre beitragen.

Wir legen besonderen Wert darauf, dass alle Nutzer:innen bei [reliverse.social](https://reliverse.social) Kommunikation gewalt- und unterdrückungsfrei erfahren, unabhängig von Religion, Bekenntnis, Kenntnis, Erfahrung, Geschlecht(-sidentität und -ausdruck), sexueller Orientierung, Einschränkung und Behinderung, persönlicher Erscheinung, Körpergröße, Ethnie, Alter oder Nationalität.

Wir definieren Gewalt oder Unterdrückung als jede Form von Sprache oder Handlung, die gesellschaftliche (Vor-)Herrschaft über andere in irgendeiner Form befürwortet, unterstützt, stärkt oder damit sympathisiert.

### 3.2 Unerwünschte Verhaltensweisen

Folgende Verhaltensweisen sind bei uns unerwünscht:

- Belästigung, Stalking, Doxxing
- Rassismus, Sexismus, Beschimpfungen oder deren Befürwortung
- Diskriminierung von Geschlechtern und Minderheiten
- Gewalttätige nationalistische Propaganda, Nazi-Symbolismus oder Förderung der Ideologie des Nationalsozialismus
- Kontaktaufnahme zu Menschen, die deutlich gemacht haben, dass sie keinen Kontakt wollen; Dogpiling
- Posten und Verbreiten von sexuellem Inhalt oder Gewaltdarstellungen
- automatische Postings (Spam)
- Bots, die ohne explizite Anfrage von Nutzer:innen mit Nutzer:innen interagieren
- Vortäuschen einer falschen Identität

Diese Liste ist nicht abschließend. Wenn wir feststellen, dass andere Verhaltensweisen, die wir für unerwünscht halten, mehrfach vorkommen, ergänzen wir diese Liste. Wir können aber auch in Einzelfällen Verhaltensweisen, die in der Liste noch nicht genannt sind, für unerwünscht erklären.

Informationen zur Bildung über freie Software und Infos über Angebote, die kirchlichen, diakonischen, mildtätigen oder gemeinnützigen Zwecken dienen, sind bei [reliverse.social](https://reliverse.social) willkommen. Werbung für andere Produkte, Angebote oder Zwecke ist bei uns unerwünscht.

### 3.3 Meldung von unerwünschtem Verhalten

Wenn du das Verhalten anderer Nutzer:innen für unerwünscht hältst, kannst du den „Report“-Button verwenden, uns per Mail an [reliverse@comenius.de](mailto:reliverse@comenius.de) oder per Nachricht an den Account [@joerglohrer@reliverse.social](https://reliverse.social/@joerglohrer) benachrichtigen. Wir überprüfen alle Meldungen und ergreifen ggf. angemessene Maßnahmen. Wir schützen unter allen Umständen die Identität der meldenden Nutzer:innen.

### 3.4 Instanz-Sprachen: Deutsch, Englisch

Wir wollen den Austausch und die Kommunikation zu religiöser Bildung unterstützen, auch in der Föderation mit Mastodon-Instanzen auf der ganzen Welt. Wir wünschen uns, dass Menschen in vielen Sprachen über [reliverse.social](https://reliverse.social) kommunizieren. Wir müssen aber in der Lage sein, Maßnahmen gegen unerwünschtes Verhalten in Fremdsprachen zu ergreifen. Dazu brauchen wir Moderatoren, die die Sprachen der Beiträge auf unserer Instanz verstehen.

Zur Zeit sind deshalb bei [reliverse.social](https://reliverse.social) **nur Deutsch und Englisch als Instanzsprachen zugelassen.** Weitere Sprachen können aufgenommen werden, wenn sich sprachlich kompetente Ehrenamtliche finden, die sich bereit erklären, Beiträge in den entsprechenden Sprachen zu moderieren.

In einzelnen Fällen werden Beiträge auf Italienisch, Latein, Altgriechisch und Althebräisch aus religiösem und/oder wissenschaftlichen Interesse toleriert.

<h2 id="datenschutz">4. Datenschutzinformation</h2>

Diese Datenschutzhinweise klären dich darüber auf, welche Informationen bei der Nutzung der Mastodon-Instanz verarbeitet werden und welche Rechte du gegenüber uns, dem [Comenius-Institut, Evangelische Arbeitsstätte für Erziehungswissenschaft e.V.](https://comenius.de/), als Betreiber der Instanz hast. Alle hier dargestellten Ausführungen und die Aufklärung über deine Rechte richten sich nach der seit dem 25.05.2018 geltenden europäischen Datenschutz-Grundverordnung (DSGVO).

### 4.1 Datenschutzbeauftragter

Das Comenius-Institut hat einen externen Beauftragten für den Datenschutz bestellt. Die Kontaktadresse ist hierfür  
  
Marco Tessendorf  
procado Consulting, IT- & Medienservice GmbH  
Warschauer Str. 58a  
10243 Berlin  
ds-comenius@procado.de  

Die zuständige Aufsichtbehörde ist der Beauftragte für den Datenschutz der Evangelischen Kirche in Deutschland https://datenschutz.ekd.de/

### 4.2 Datenverarbeitung bei Aufruf der Startseite, Registrierungsmaske und Nutzung im Allgemeinen

Sobald du die Startseite von [reliverse.social](https://reliverse.social) aufrufst, um dich zu registrieren oder auch während der Nutzung im Allgemeinen, wird eine Verbindung zum Webserver aufgebaut, auf dem diese Instanz betrieben wird. Um dem Browser deines Endgeräts die Seite anzeigen zu können, werden dabei entsprechend dem HTTP und TCP/IP Protokoll gewisse Daten verarbeitet. Dazu gehören:

- die IP-Adresse deines Internetanschlusses,
- der Zeitpunkt deines Zugriffs auf die Webseite,
- die Adresse der aufgerufenen Webseite,
- Informationen zu deinem Gerät, z.B.
  - die Betriebssystemversion deines PCs, Tablets oder Smartphones,
  - der von dir genutzte Internetbrowser oder die genutzte App,
  - der Gerätetyp (z.B. „Mobile“),
  - bevorzugte Sprache(n),
  - die Displayauflösung deines Geräts,
  - der Standort deines Geräts,
- sowie evtl. die verweisende Webseite (Referrer).

Diese technischen Daten werden verarbeitet, damit diese Webseite von dem Webserver an dein Endgerät übermittelt und von deinem Browser richtig verarbeitet und angezeigt werden kann. Nach jedem Seitenaufruf werden einige dieser Daten in Logfiles gespeichert, die für die Wartung und Sicherheit des Servers erforderlich sind. Nach 8 Tagen werden die Logfiles gelöscht. Zugriff auf die Daten haben nur unsere Serveradministratoren und unser Webhoster der als Auftragsverarbeiter unseren Anweisungen unterliegt und sich vertraglich zur Einhaltung technischer und organisatorischer Sicherungsmaßnahmen verpflichtet hat.

Die Datenverarbeitung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) 
DSGVO im erforderlichen Umfang, um dir den Aufruf der Startseite sowie die Registrierung im Rahmen des Nutzungsverhältnisses zwischen uns (als Betreiber der Instanz) und dir (als Nutzer:in) zu ermöglichen sowie zur Erfüllung der Verpflichtung zur Ergreifung technisch-organisatorischer Schutzmaßnahmen.

### 4.3 Datenverarbeitung bei der Registrierung

Im Rahmen der Registrierung und zur Konfiguration deines Nutzerkontos verarbeiten wir grundlegende Kontoinformationen: Dazu zählen Benutzername, E-Mail-Adresse und Passwort. Du kannst auch zusätzliche Profilinformationen wie etwa einen Anzeigenamen oder eine Biografie eingeben, ein Profilbild oder ein Headerbild hochladen, dein Konto mit einem zweiten Sicherheitsfaktor schützen und einstellen, dass andere Nutzer:innen blockiert oder stummgeschaltet werden sollen. Der Benutzername, der Anzeigename, die Biografie, das Profilbild und das Headerbild werden dann immer öffentlich angezeigt.

Die Datenverarbeitung im Rahmen der Registrierung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) DSGVO im erforderlichen Umfang, um dir die Registrierung und Konfiguration im Rahmen des Nutzungsverhältnisses zu ermöglichen.

### 4.4 Datenverarbeitung bei der Nutzung deines Mastodon-Kontos

- *Beiträge, Folge- und andere öffentliche Informationen:* Die Liste der Leute, denen du folgst, wird öffentlich gezeigt, das gleiche gilt für deine Folgenden (Follower). Sobald du eine Nachricht übermittelst, wird das Datum und die Uhrzeit gemeinsam mit der Information, welche Anwendung du dafür verwendet hast, gespeichert. Nachrichten können Medienanhänge enthalten, etwa Bilder und Videos. Öffentliche und ungelistete Beiträge sind öffentlich verfügbar. Sobald du einen Beitrag auf deinem Profil anpinnst, ist dieser ebenfalls öffentlich verfügbar. Deine Beiträge werden an deine Folgenden ausgeliefert, was in manchen Fällen bedeutet, dass sie an andere Server (andere Instanzen) ausgeliefert werden und dort Kopien gespeichert werden. Sobald du Beiträge löschst, wird dies ebenso an deine Follower ausgeliefert. Die Handlungen des Teilens und Favorisieren eines anderen Beitrages sind immer öffentlich.
- *Direkte und „Nur Folgende“-Beiträge:* Alle Beiträge werden auf dem Server gespeichert und verarbeitet. „Nur Folgende“-Beiträge werden an deine Folgenden und an Benutzerinnen, die du erwähnst, ausgeliefert, direkte Beiträge („Direktnachrichten“) nur an in ihnen erwähnte Benutzerinnen. Direktnachrichten sind allerdings nicht Ende-zu-Ende verschlüsselt und daher auch durch uns sowie ggf. den Serverbetreibenden der Instanz der Empfänger:in einsehbar. Wir können nur sicherstellen, dass Unbefugte keinen Zugang zu den auf unserer Instanz gespeicherten Beiträge haben, jedoch könnten andere Server/Instanzen dabei scheitern. Deswegen ist es wichtig, die Instanz, zu denen deine Folgenden gehören, zu überprüfen. Du kannst auch eine Option in den Einstellungen umschalten, um neue Folgende manuell anzunehmen oder abzuweisen. Bitte beachte, dass die Betreibenden des Servers und jedes empfangenden Servers solche Nachrichten anschauen könnten und dass Empfänger:innen von diesen eine Bildschirmkopie erstellen könnten, sie kopieren oder anderweitig weiterverteilen könnten. Teile daher keine sensiblen Informationen über Mastodon.
- *Internet Protocol-Adressen (IP-Adressen) und andere Metadaten:* Sobald du dich anmeldest, erfasst Mastodon sowohl die IP-Adresse, von der aus du dich anmeldest, als auch den Namen deiner Browseranwendung und ggf. weitere Geräteinformationen, die dein Browser übermittelt. In den Einstellungen kannst du alle angemeldeten Sitzungen (Sessions) überprüfen und widerrufen. Die letzte verwendete IP-Adresse und die letzten dabei vom Browser übermittelten Informationen speichern wir bis zu 3 Monate lang.

Die Datenverarbeitung im Rahmen der Nutzung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) DSGVO im erforderlichen Umfang, um dir die Nutzung deines Mastodon-Kontos im Rahmen des Nutzungsverhältnisses zu ermöglichen.

### 4.5 Nutzung von Cookies und weiteren Techniken zur Speicherung von Daten im Browser

Mastodon verwendet Cookies, IndexedDB, LocalStorage und SessionStorage (d.h. kleine Textdateien und Datenbanken, die Informationen zu deinem Mastodon-Konto auf deinem Endgerät speichern), um Kern- und Komfortfunktionen bereitzustellen. Wenn du nicht bei reliverse.social registriert bist, werden alle Daten, die mit diesen Techniken gespeichert wurden, gelöscht, wenn du deinen Browser beendest. Wenn du ein registriertes Konto hast, bleiben Daten gespeichert, damit  die Mastodon-Instanz deinen Browser wiederkennen und mit deinem Konto verknüpfen kann. Du kannst diese Daten aber jederzeit mit den entsprechenden Funktionen deines Browsers löschen – dann musst du dich beim nächsten Aufruf von [reliverse.social](https://reliverse.social) mit den Anmeldedaten für dein Benutzerkonto neu einloggen.

### 4.6 Deine Rechte

Du hast ein Recht auf

- **Auskunft** darüber, welche Daten wir zu dir gespeichert haben, einschließlich einer Kopie dieser Daten (gemäß Art. 15 DSGVO),

- **Berichtigung** falscher Daten (gemäß Art. 16 DSGVO),

- **Löschung** („Vergessen werden“) oder **Einschränkung der Verarbeitung** (gemäß Art. 17-18 DSGVO),

- **Widerspruch** (gemäß Art. 21 DSGVO),

- Erhalt deiner **Daten in einem strukturierten, gängigen und maschinenlesbaren Format**, das du an einen anderen Mastodon-Server übermitteln kannst oder ein Recht darauf, dass wir deine Daten direkt an einen anderen Dienst übermitteln, soweit dies technisch möglich ist (gemäß Art. 20 DSGVO).

Du kannst dein Mastodon-Konto löschen, wenn du eingeloggt bist und *„Profil bearbeiten“* anklickst. Ganz unten findest du dann den Abschnitt *„Konto löschen“.*

Du kannst Kopien deiner Daten in strukturierten, gängigen und maschinenlesbaren Formaten alle 7 Tage herunterladen, wenn du eingeloggt bist und *„Profil bearbeiten“, „Importieren und Exportieren“* sowie *„Datenexport“* anklickst.

Wende dich bitte per E-Mail an [reliverse@comenius.de](mailto:reliverse@comenius.de), wenn du eins der anderen Rechte wahrnehmen willst.

## Hinweise zu diesem Dokument

Wir verbessern [reliverse.social](https://reliverse.social) kontinuierlich. Wenn sich dabei Veränderungen bei der Datenverarbeitung oder Änderungsbedarf bei Nutzungsbedingungen und Verhaltenskodex ergeben, passen wir diese Hinweise an. Wir empfehlen dir deshalb, ab und zu wieder hier vorbeizuschauen. Bisher gab es folgende Versionen:

- 0.1 27.01.2023

Wenn du Verbesserungsvorschläge hast, freuen wir uns darüber. Wir freuen uns auch über Fragen, weil sie uns helfen, den Text zu verbessern und verständlicher zu machen. Schick Fragen oder Verbesserungsvorschläge bitte per E-Mail an [reliverse@comenius.de](mailto:reliverse@comenius.de) oder schreibe deine Vorschläge in unser [Codeberg-Repository](https://codeberg.org/Comenius-Institut/reliverse).

### Nutzung des Textes unter CC-Lizenz

Wenn du selbst eine Mastodon-Instanz betreibst, kannst du unseren Text unter der CreativeCommons-Lizenz „CC BY-SA 3.0 DE“ übernehmen und anpassen. Bedingungen dafür sind:

- Das "Comenius-Institut" sowie „synod.im / LUKi e.V.“ und die unten genannten Lizenzgeber für Texte, die wir verwendet haben, müssen als Quelle angegeben werden.
- Abgeleitete Werke müssen ebenfalls unter die CC BY-SA oder eine vergleichbare Lizenz gestellt werden.

Weitere Informationen und Verweise zum vollständigen Text der Lizenz findest du bei [https://creativecommons.org/licenses/by-sa/3.0/de/](https://creativecommons.org/licenses/by-sa/3.0/de/)

Wir empfehlen dir aber, genau zu prüfen, ob unsere Nutzungsbedingungen und unsere Datenschutzerklärung auch für deine Instanz passen. Die Freigabe dieses Textes zur Nutzung und Anpassung ist keine Rechtsberatung. Frag im Zweifelsfall einen Datenschutzbeauftragten oder einen Rechtsanwalt.

### Texte, die wir verwendet haben

- Die Hinweise basieren in der Grundlage auf die [Datenschutzinformationen von kirche.social](https://kirche.social/privacy-policy) in der Version vom 22.11.2022 und stehen unter der oben genannten CreativeCommons-Lizenz "[CC BY-SA 3.0 DE](https://creativecommons.org/licenses/by-sa/3.0/de/)" unter Namensnennung von  „synod.im / [LUKi e.V.](https://luki.org/)“ 
- Die Liste unerwünschter Verhaltensweisen basiert auf einer englischen Liste von [chaos.social](https://chaos.social/about/more) (freigegeben unter CC-BY-Lizenz) und deren Übersetzung ins Deutsche von [Alexander Kallenbach / mastodonten.de](https://mastodonten.de/about/more) (freigegeben unter CC-BY-SA-Lizenz).
- Die Datenschutzerklärung basiert auf den Datenschutzhinweisen von [Malte Engeler / legal.social](https://legal.social/terms#Datenschutzhinweise) (freigegeben unter CC-BY-SA-3.0-Lizenz).
- Die Grundsatzbestimmungen des Verhaltenskodex wurden vom [Code of Conduct von Fosstodon](https://hub.fosstodon.org/code-of-conduct) inspiriert.
- https://github.com/luki-ev/doc-legal/blob/main/reliverse.social/terms.md